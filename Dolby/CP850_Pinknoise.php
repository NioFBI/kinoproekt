﻿<?php
//$global_ip = '192.168.11.18';
//$global_port = '9090';


if(isset($argv[1]) AND isset($argv[2]) AND isset($argv[3])){
	if($argv[3]=='l' OR $argv[3]=='r' OR $argv[3]=='c' OR $argv[3]=='lfe'){
		if($argv[3]=='l'){$cnt='0';}
		if($argv[3]=='r'){$cnt='1';}
		if($argv[3]=='c'){$cnt='2';}
		if($argv[3]=='lfe'){$cnt='3';}
		$input = 'pinkNoise';
		$type = 'speaker';
		$cn = '<speakerIndex>'.$cnt.'</speakerIndex>';
	}elseif($argv[3]!='off'){
		$input = 'pinkNoise';
		$type = 'array';
		$cn = '<array>'.$argv[3].'</array>';
	}else{
		$input = 'off';
		$type = 'speaker';
		$cn = '<speakerIndex>3</speakerIndex>';		
	}
	$global_ip = $argv[1];
	$global_port = $argv[2];
	
}else{echo "No Parameters\n";exit();}




function post_send($global_ip,$global_port,$url_data,$xml_data){
	$url = "http://".$global_ip.":".$global_port."/cp/ws/smi/v1/services/SystemManagement";
	$page = "/cp/ws/smi/v1/services/SystemManagement";
	$headers = array(
		"POST ".$page." HTTP/1.0",
		'Accept-Encoding: gzip,deflate',
		"Content-type: text/xml;charset=UTF-8",
		'SOAPAction: "'.$url_data.'"',
		'Host: '.$global_ip.':'.$global_port.'',
		'Connection: Keep-Alive',
		'User-Agent: Apache-HttpClient/4.1.1 (java 1.5)',
		"Content-length: ".strlen($xml_data)
	);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	// Apply the XML to our curl call
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

	$data = curl_exec($ch);

	if(curl_errno($ch)){
		print "Error: " . curl_error($ch);exit();
	}else{
		// Show me the result
		//var_dump($data);
		curl_close($ch);
		
	}
}



	$xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://www.dolby.com/cp/ws/smi/v1_0"><soapenv:Header/><soapenv:Body><v1:setTestSignalOutputRequest><outputMode>'.$type.'</outputMode>'.$cn.'</v1:setTestSignalOutputRequest></soapenv:Body></soapenv:Envelope>';
	$url_data = 'http://'.$global_ip.':'.$global_port.'/cp/ws/smi/v1/setTestSignalOutput';

	post_send($global_ip,$global_port,$url_data,$xml_data);


	$xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://www.dolby.com/cp/ws/smi/v1_0"><soapenv:Header/><soapenv:Body><v1:setTestSignalModeRequest><signalType>'.$input.'</signalType></v1:setTestSignalModeRequest></soapenv:Body></soapenv:Envelope>';
	$url_data = 'http://'.$global_ip.':'.$global_port.'/cp/ws/smi/v1/setTestSignalMode';

	post_send($global_ip,$global_port,$url_data,$xml_data);
	
	echo "OK\n";
?>